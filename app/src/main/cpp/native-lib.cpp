#include <jni.h>
#include <string>
//The file "sensitive-strings.cpp" should be not included in the repository, uploaded for the recruitment process.
#import "sensitive-strings.cpp"
//
// Created by MichalDiner on 2.08.2018.
//
extern "C"

JNIEXPORT jstring JNICALL
Java_pl_mdiner_jsonfetcher_json_TumblrJson_getConsumerKey(
        JNIEnv *env,
        jobject /* this */) {

    std::string mNativeConsumerKey = consumerKey;
    return env->NewStringUTF(mNativeConsumerKey.c_str());
}
extern "C"
JNIEXPORT jstring JNICALL
Java_pl_mdiner_jsonfetcher_json_TumblrJson_getConsumerSecret(
        JNIEnv *env,
        jobject /* this */) {

    std::string mNativeConsumerSecret = consumerSecret;
    return env->NewStringUTF(mNativeConsumerSecret.c_str());
}
extern "C"
JNIEXPORT jstring JNICALL
Java_pl_mdiner_jsonfetcher_json_TumblrJson_getOauthToken(
        JNIEnv *env,
        jobject /* this */) {

    std::string mOauthToken = oauthToken;
    return env->NewStringUTF(mOauthToken.c_str());
}
extern "C"
JNIEXPORT jstring JNICALL
Java_pl_mdiner_jsonfetcher_json_TumblrJson_getOauthTokenSecret(
        JNIEnv *env,
        jobject /* this */) {

    std::string mOauthTokenSecret = oauthTokenSecret;
    return env->NewStringUTF(mOauthTokenSecret.c_str());
}

