package pl.mdiner.jsonfetcher.json;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;

import com.tumblr.jumblr.JumblrClient;
import com.tumblr.jumblr.exceptions.JumblrException;
import com.tumblr.jumblr.types.PhotoPost;
import com.tumblr.jumblr.types.Post;
import com.tumblr.jumblr.types.TextPost;
import com.tumblr.jumblr.types.VideoPost;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.mdiner.jsonfetcher.ui.fragments.BlogFragment;
import pl.mdiner.jsonfetcher.utils.Blog;

import static android.util.Base64.decode;

/**
 * Created by Michał Diner on 14.08.2018.
 */
public class TumblrJson extends AsyncTask<Void, Void, Boolean> {

    public static final int TEXT = 0;
    public static final int PHOTO = 1;
    public static final int VIDEO = 2;
    private static final String TUMBLR_COM = ".tumblr.com";

    /* Due to no real way of hiding sensitive data in Android and only many
     ways to obfuscate - ConsumerKey and ConsumerSecret is taken from
     native C++ encoded in base64. */

    static {
        System.loadLibrary("native-lib");
    }

    private final String mBlogName;
    private final int mType;

    private List<Post> photoPosts;
    private List<Post> textPosts;
    private List<Post> videoPosts;


    public TumblrJson(String blogName, int type) {
        this.mType = type;
        this.mBlogName = blogName;
    }

    public native String getConsumerKey();
    public native String getConsumerSecret();
    public native String getOauthToken();
    public native String getOauthTokenSecret();

    private JumblrClient client;
    private OnTaskCompleted mListener;

    @Override
    protected void onPreExecute() {
        String consumerKey = getEncodedString(getConsumerKey());
        String consumerSecret = getEncodedString(getConsumerSecret());
        String oauthToken = getEncodedString(getOauthToken());
        String oauthTokenSecret = getEncodedString(getOauthTokenSecret());

        client = new JumblrClient(consumerKey, consumerSecret);
        client.setToken(oauthToken, oauthTokenSecret);
    }

    @NonNull
    private String getEncodedString(String string) {
        return new String(decode(string, Base64.DEFAULT));
    }

    public interface OnTaskCompleted {
        void onTaskCompleted(ArrayList<Blog> list);

        void onError();
    }


    public void onFinish(OnTaskCompleted onComplete) {
        this.mListener = onComplete;
    }

    protected Boolean doInBackground(Void... args){
        try{

            Map<String, Object> textParams = new HashMap<String, Object>();
            textParams.put("type", "text");

            Map<String, Object> photoParams = new HashMap<String, Object>();
            photoParams.put("type", "photo");

            Map<String, Object> videoParams = new HashMap<String, Object>();
            videoParams.put("type", "video");

            switch(mType){
                case TEXT:
                    textPosts = client.blogPosts(mBlogName + TUMBLR_COM, textParams);
                    break;
                case PHOTO:
                    photoPosts = client.blogPosts(mBlogName + TUMBLR_COM, photoParams);
                    break;
                case VIDEO:
                    videoPosts = client.blogPosts(mBlogName + TUMBLR_COM, videoParams);
                    break;
                default:
                    mListener.onError();
                    break;
            }
        }
        catch (JumblrException e){
            Log.d("JumblrException ", e.getMessage());
            mListener.onError();
            return false;
        }
        return true;
    }

    @Override
    protected void onPostExecute(final Boolean success){
        if(success){
            ArrayList<Blog> blogContentList = new ArrayList<>();
            switch(mType){
                case TEXT:
                    textLoop(blogContentList);
                    mListener.onTaskCompleted(blogContentList);
                    break;
                case PHOTO:
                    photoLoop(blogContentList);
                    mListener.onTaskCompleted(blogContentList);
                    break;
                case VIDEO:
                    videoLoop(blogContentList);
                    mListener.onTaskCompleted(blogContentList);
                    break;
                default:
                    mListener.onError();
                    break;
            }
        }
    }

    private void textLoop(ArrayList<Blog> blogContentList) {
        for (int i = 0; i < textPosts.size(); i++) {
            Blog blog = new Blog();
            TextPost post = (TextPost) textPosts.get(i);
            String content = post.getBody();
            String postUrl = textPosts.get(i).getPostUrl();
            String blogName = textPosts.get(i).getBlogName();
            String sourceUrl = textPosts.get(i).getSourceUrl();
            BlogFragment.sBlogTitle = blogName;
            blog.setContent(content);
            blog.setPostUrl(postUrl);
            blog.setSourceUrl(sourceUrl);
            blogContentList.add(blog);
        }
    }

    private void photoLoop(ArrayList<Blog> blogContentList) {
        for (int i = 0; i < photoPosts.size(); i++) {
            PhotoPost post = (PhotoPost) photoPosts.get(i);
            String photoCaption = post.getCaption();
            String photoUrl = post.getPhotos().get(0).getOriginalSize().getUrl();
            String postUrl = photoPosts.get(i).getPostUrl();
            String blogName = photoPosts.get(i).getBlogName();
            String sourceUrl = photoPosts.get(i).getSourceUrl();
            Blog blog = new Blog();
            BlogFragment.sBlogTitle = blogName;
            blog.setPhotoCaption(photoCaption);
            blog.setPhotoUrl(photoUrl);
            blog.setPostUrl(postUrl);
            blog.setSourceUrl(sourceUrl);
            blogContentList.add(blog);
        }
    }

    private void videoLoop(ArrayList<Blog> blogContentList) {
        for (int i = 0; i < videoPosts.size(); i++) {
            VideoPost post = (VideoPost) videoPosts.get(i);
            String videoPhoto = post.getThumbnailUrl();
            String videoUrl = post.getPermalinkUrl();
            String videoCaption = post.getCaption();
            String postUrl = videoPosts.get(i).getPostUrl();
            String blogName = videoPosts.get(i).getBlogName();
            String sourceUrl = videoPosts.get(i).getSourceUrl();
            Blog blog = new Blog();
            BlogFragment.sBlogTitle = blogName;
            blog.setVideoPhoto(videoPhoto);
            blog.setVideoCaption(videoCaption);
            blog.setVideoUrl(videoUrl);
            blog.setPostUrl(postUrl);
            blog.setSourceUrl(sourceUrl);
            blogContentList.add(blog);
        }
    }
}