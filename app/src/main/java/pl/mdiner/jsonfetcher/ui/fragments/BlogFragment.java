package pl.mdiner.jsonfetcher.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import pl.mdiner.jsonfetcher.R;
import pl.mdiner.jsonfetcher.adapters.BlogAdapter;
import pl.mdiner.jsonfetcher.json.TumblrJson;
import pl.mdiner.jsonfetcher.ui.activities.MainActivity;
import pl.mdiner.jsonfetcher.utils.Blog;

/**
 * Created by Michał Diner on 22.08.2018.
 */
public class BlogFragment extends Fragment {

    @BindView(R.id.rv_blog_content)
    RecyclerView rvBlogContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout mSwipeRefresh;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    Unbinder unbinder;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbarLayout;

    private BlogAdapter mAdapter;
    public static String sBlogTitle;

    public BlogFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_blog, container, false);
        unbinder = ButterKnife.bind(this, view);

        rvBlogContent.setHasFixedSize(true);
        LinearLayoutManager playlistLinearLayout = new LinearLayoutManager(getContext());
        rvBlogContent.setLayoutManager(playlistLinearLayout);

        mSwipeRefresh.setColorSchemeResources(R.color.theme_primary, R.color.theme_primary_dark);
        mSwipeRefresh.canChildScrollUp();
        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                mAdapter.notifyDataSetChanged();
                mSwipeRefresh.setRefreshing(true);
                loadFeed();
            }
        });
        if (!isNetworkAvailable()) {
            sendToMainActivity();
        } else {
            loadFeed();
        }

        return view;
    }

    private void sendToMainActivity() {
        Intent goBack = new Intent(getContext(), MainActivity.class);
        if (getActivity() != null) {
            getActivity().startActivity(goBack);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdapter != null)
            mAdapter.notifyDataSetChanged();
    }

    private void loadFeed() {
        if (!mSwipeRefresh.isRefreshing())
            mProgressBar.setVisibility(View.VISIBLE);

        TumblrJson json = new TumblrJson(MainFragment.blogName, MainFragment.typeOfBlog);
        json.execute();
        json.onFinish(new TumblrJson.OnTaskCompleted() {
            @Override
            public void onTaskCompleted(ArrayList<Blog> list) {
                mAdapter = new BlogAdapter(list, getContext());
                rvBlogContent.setAdapter(mAdapter);
                mProgressBar.setVisibility(View.GONE);
                mSwipeRefresh.setRefreshing(false);
                toolbarLayout.setTitle(sBlogTitle);
            }

            @Override
            public void onError() {
                Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mProgressBar.setVisibility(View.GONE);
                        mSwipeRefresh.setRefreshing(false);
                        Toast.makeText(getContext(), getString(R.string.unable_to_load_data),
                                Toast.LENGTH_LONG).show();
                        sendToMainActivity();
                    }
                });
            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) Objects.requireNonNull(getActivity())
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        if (mAdapter != null)
            mAdapter.clearData();
    }
}
