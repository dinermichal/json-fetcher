package pl.mdiner.jsonfetcher.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import pl.mdiner.jsonfetcher.R;
import pl.mdiner.jsonfetcher.ui.activities.MainActivity;

import static android.text.TextUtils.isEmpty;
import static pl.mdiner.jsonfetcher.ui.activities.MainActivity.EXTRA_CIRCULAR_REVEAL_X;
import static pl.mdiner.jsonfetcher.ui.activities.MainActivity.EXTRA_CIRCULAR_REVEAL_Y;
import static pl.mdiner.jsonfetcher.ui.activities.MainActivity.WHERE_TO;

/**
 * Created by Michał Diner on 22.08.2018.
 */
public class MainFragment extends Fragment {

    public static final int BLOG = 1;
    @BindView(R.id.et_blog_name_edit)
    EditText etBlogNameEdit;
    @BindView(R.id.spinner)
    Spinner mSpinner;

    private Unbinder mUnbinder;
    public static int typeOfBlog;
    public static String blogName;

    public MainFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                typeOfBlog = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private boolean validateForm() {
        boolean valid = true;
        blogName = etBlogNameEdit.getText().toString();
        if (isEmpty(blogName)) {
            etBlogNameEdit.setError(getString(R.string.required));
            etBlogNameEdit.requestFocus();
            valid = false;
        } else {
            etBlogNameEdit.setError(null);
        }
        return valid;
    }

    public void presentActivity(View view) {
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation((Activity) getContext(), view, "transition");
        int revealX = (int) (view.getX() + view.getWidth() / 2);
        int revealY = (int) (view.getY() + view.getHeight() / 2);

        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.putExtra(EXTRA_CIRCULAR_REVEAL_X, revealX);
        intent.putExtra(EXTRA_CIRCULAR_REVEAL_Y, revealY);
        intent.putExtra(WHERE_TO, BLOG);
        ActivityCompat.startActivity(getContext(), intent, options.toBundle());
    }

    @OnClick(R.id.btn_fetch_json)
    public void onFetchJsonClicked(View view) {
        if (validateForm()) {
            if (isNetworkAvailable()) {
                presentActivity(view);
            } else {
                Toast.makeText(getContext(), R.string.alert_message, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
