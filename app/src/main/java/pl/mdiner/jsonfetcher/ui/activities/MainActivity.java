package pl.mdiner.jsonfetcher.ui.activities;

import android.animation.Animator;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.mdiner.jsonfetcher.R;
import pl.mdiner.jsonfetcher.ui.fragments.BlogFragment;
import pl.mdiner.jsonfetcher.ui.fragments.MainFragment;

import static pl.mdiner.jsonfetcher.ui.fragments.MainFragment.BLOG;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_CIRCULAR_REVEAL_X = "EXTRA_CIRCULAR_REVEAL_X";
    public static final String EXTRA_CIRCULAR_REVEAL_Y = "EXTRA_CIRCULAR_REVEAL_Y";
    public static final String WHERE_TO = "where_to";
    private int mWhereTo;
    private int revealX;
    private int revealY;

    @BindView(R.id.fragment_container)
    RelativeLayout fragmentContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.container);
        ButterKnife.bind(this);

        Bundle intent = getIntent().getExtras();
        if (intent != null) {
            mWhereTo = intent.getInt(WHERE_TO);
            revealX = intent.getInt(EXTRA_CIRCULAR_REVEAL_X, 0);
            revealY = intent.getInt(EXTRA_CIRCULAR_REVEAL_Y, 0);
        }
        if (savedInstanceState == null
                && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (mWhereTo == BLOG) {
                fragmentContainer.setVisibility(View.INVISIBLE);
                ViewTreeObserver viewTreeObserver = fragmentContainer.getViewTreeObserver();
                if (viewTreeObserver.isAlive()) {
                    viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            revealActivity(revealX, revealY);
                            fragmentContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
                    });
                }
            } else {
                fragmentContainer.setVisibility(View.VISIBLE);
            }
        }

        switch (mWhereTo) {
            case 1:
                Fragment blogFragment = new BlogFragment();
                openFragment(blogFragment);
                break;
            default:
                Fragment mainFragment = new MainFragment();
                openFragment(mainFragment);
                break;
        }

    }

    //Helper method for opening the fragments.
    private void openFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();
    }

    protected void revealActivity(int x, int y) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            float finalRadius = (float) (Math.max(fragmentContainer.getWidth(), fragmentContainer.getHeight()) * 1.1);

            Animator circularReveal = ViewAnimationUtils.createCircularReveal(fragmentContainer, x, y, 0, finalRadius);
            circularReveal.setDuration(700);
            circularReveal.setInterpolator(new AccelerateInterpolator());

            fragmentContainer.setVisibility(View.VISIBLE);
            circularReveal.start();
        } else {
            finish();
        }
    }
}
