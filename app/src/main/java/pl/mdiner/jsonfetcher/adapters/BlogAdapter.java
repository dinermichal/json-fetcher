package pl.mdiner.jsonfetcher.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.jsoup.Jsoup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.mdiner.jsonfetcher.R;
import pl.mdiner.jsonfetcher.ui.fragments.MainFragment;
import pl.mdiner.jsonfetcher.utils.Blog;

import static android.text.TextUtils.isEmpty;
import static pl.mdiner.jsonfetcher.json.TumblrJson.PHOTO;
import static pl.mdiner.jsonfetcher.json.TumblrJson.TEXT;
import static pl.mdiner.jsonfetcher.json.TumblrJson.VIDEO;

/**
 * Created by Michał Diner on 22.08.2018.
 */
public class BlogAdapter extends RecyclerView.Adapter<BlogAdapter.BlogViewHolder> {

    private final ArrayList<Blog> mBlogContentList;
    private final Context mContext;

    public BlogAdapter(ArrayList<Blog> sBlogContentList, Context context) {
        this.mBlogContentList = sBlogContentList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public BlogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.blog_card, parent, false);
        return new BlogViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final BlogViewHolder holder, final int position) {
        final Blog blogs = mBlogContentList.get(position);
        switch (MainFragment.typeOfBlog) {
            case TEXT:
                textSetter(holder, blogs);
                break;
            case PHOTO:
                photoSetter(holder, blogs);
                break;
            case VIDEO:
                videoSetter(holder, blogs);
                break;
        }
        if (!isEmpty(blogs.getSourceUrl())) {
            holder.tvSource.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openUrl(blogs.getSourceUrl());
                }
            });
        } else {
            holder.tvSource.setVisibility(View.GONE);
        }
    }

    private void openUrl(String url) {
        Intent myIntent = new Intent(android.content.Intent.ACTION_VIEW);
        String extension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(url);
        String mimetype = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        myIntent.setDataAndType(Uri.parse(url), mimetype);
        mContext.startActivity(myIntent);
    }

    private void videoSetter(@NonNull final BlogViewHolder holder, final Blog blogs) {
        holder.ivBlogVideoThumbnail.setVisibility(View.VISIBLE);
        holder.ivBlogPhoto.setVisibility(View.GONE);
        holder.tvBlogText.setVisibility(View.GONE);
        Glide.with(mContext)
                .load(blogs.getVideoPhoto())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.ivBlogVideoThumbnail.setVisibility(View.GONE);
                        holder.tvBlogText.setVisibility(View.VISIBLE);
                        holder.tvBlogText.setText(R.string.somethings_wrong);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.mProgressBar.setVisibility(View.GONE);
                        if (!isEmpty(blogs.getVideoCaption())) {
                            holder.cvBlogCaptions.setVisibility(View.VISIBLE);
                            holder.tvBlogCaptions.setText(Jsoup.parse(blogs.getVideoCaption()).body().text());
                        }
                        return false;
                    }
                })
                .into(holder.ivBlogVideoThumbnail);
        holder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                openUrl(blogs.getVideoUrl());
                } catch (Exception e) {
                    Log.d("Video click, ", e.toString());
                }
            }
        });
    }

    private void photoSetter(@NonNull final BlogViewHolder holder, final Blog blogs) {
        holder.ivBlogPhoto.setVisibility(View.VISIBLE);
        Glide.with(mContext)
                .load(blogs.getPhotoUrl())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.ivBlogPhoto.setVisibility(View.GONE);
                        holder.tvBlogText.setVisibility(View.VISIBLE);
                        holder.tvBlogText.setText(R.string.somethings_wrong);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.mProgressBar.setVisibility(View.GONE);
                        if (!isEmpty(blogs.getPhotoCaption())) {
                            holder.cvBlogCaptions.setVisibility(View.VISIBLE);
                            holder.tvBlogCaptions.setText(Jsoup.parse(blogs.getPhotoCaption()).body().text());
                        }
                        return false;
                    }
                })
                .into(holder.ivBlogPhoto);
        holder.ivBlogPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    openUrl(blogs.getPostUrl());
                } catch (Exception e) {
                    Log.d("Photo click, ", e.toString());
                }
            }
        });
    }

    private void textSetter(@NonNull BlogViewHolder holder, final Blog blogs) {
        holder.mProgressBar.setVisibility(View.GONE);
        holder.tvBlogText.setText(Jsoup.parse(blogs.getContent()).body().text());
        holder.tvBlogText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(blogs.getPostUrl())));
            }
        });
    }

    public void clearData() {
        if (mBlogContentList != null)
            mBlogContentList.clear();
    }

    @Override
    public int getItemCount() {
        return mBlogContentList == null ? 0 : mBlogContentList.size();
    }

    class BlogViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.iv_blog_photo)
        ImageView ivBlogPhoto;
        @BindView(R.id.iv_blog_text)
        TextView tvBlogText;
        @BindView(R.id.progress_bar)
        ProgressBar mProgressBar;
        @BindView(R.id.tv_source)
        TextView tvSource;
        @BindView(R.id.iv_video_view)
        ImageView ivBlogVideoThumbnail;
        @BindView(R.id.card_view)
        CardView mCardView;
        @BindView(R.id.tv_caption)
        TextView tvBlogCaptions;
        @BindView(R.id.cv_caption)
        CardView cvBlogCaptions;


        BlogViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
